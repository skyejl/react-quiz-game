import React from 'react'
import ReactDom from 'react-dom'
import ControlPanel from "./ControlPanel";

ReactDom.render(
    <ControlPanel/>,
    document.getElementById('root')
)
