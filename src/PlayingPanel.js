import React from 'react';
import Result from "./Result";
import './ControlPanel.less';

class PlayingPanel extends React.Component{

    constructor(props, context) {
        super(props, context);
        this.state = {
            answer: [],
            isCorrect: false,
            isClear: this.props.clearContent
        }
        this.handleUserInput =this.handleUserInput.bind(this);
        this.handleCheckAnswer =this.handleCheckAnswer.bind(this);
    }
    handleUserInput(event) {
        this.setState({
            answer: event.target.value.split("")
        })
    }
    handleCheckAnswer() {
        let userAnswer = this.state.answer.toString();
        let result = this.props.result.toString();
        if(userAnswer === "" || result === "") {
            alert("请先输入答案或者先创建一局游戏")
        }
        if (userAnswer === result) {
            this.setState({
                isCorrect: true
            })
        }
    }
    render() {
        const isCorrect = this.state.isCorrect;
        const isAnswer =  this.state.answer.length;
        return (
            <div>
                <section className="playTop">
                    <Result result={this.state.answer }/>
                    {(isCorrect) ? (
                        <p className="success">Success</p>
                    ) : (
                        <p className="fail">Fail</p>
                    )}
                </section>
                <section className="playBottom">
                    <p>Guess Card</p>
                    <input className="userInput" type="text" maxLength={4} onChange={this.handleUserInput}/>
                    <div className="guess">
                        <button className="userGuess" onClick={this.handleCheckAnswer}>Guess</button>
                    </div>
                </section>
            </div>
        );
    }
}
export default PlayingPanel;
