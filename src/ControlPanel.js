import React from 'react';
import './ControlPanel.less';
import PlayingPanel from "./PlayingPanel";
import Result from "./Result";

class ControlPanel extends React.Component{

    constructor(props, context) {
        super(props, context);
        this.state = {
            result: [],
            displayResult: false,
            clearContent: false
        }
        this.handleGenerateNums =this.handleGenerateNums.bind(this);
        this.hideResult =this.hideResult.bind(this);
        this.handleShowAnswer =this.handleShowAnswer.bind(this);
    }

    handleGenerateNums() {
        let result = [];
        for(let i=0;i<4;i++){
            let ranNum = Math.ceil(Math.random() * 25);
            result.push(String.fromCharCode(65+ranNum));
        }
        this.setState({
            result: [...result],
            displayResult: true
        });
        this.hideResult();
    }

    hideResult() {
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
            this.setState({
                displayResult: false
            });
        }, 3000);
    }

    handleShowAnswer() {
        this.setState({
            displayResult: true,
            clearContent: true
        })
    }

    render() {
        const isDisplay = this.state.displayResult;
        return (
            <div className="panel">
                <section className="playingSection">
                    <PlayingPanel result={this.state.result} />
                </section>
                <section className="controlSection">
                    <p>New Card</p>
                    <button className="newGameButton" onClick={this.handleGenerateNums}>New Card</button>
                    {isDisplay ? (
                        <Result result={this.state.result}/>
                    ) : (
                        <div></div>
                    )}
                    <button className="showGameButton" onClick={this.handleShowAnswer}>Show Result</button>
                </section>


            </div>
        );
    }
}
export default ControlPanel;
