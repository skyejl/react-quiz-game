import React from 'react';

const Result = (props) => {
    const ResultItems = props.result.map((number, index) =>
        <input key={index} className="cell" type="text" value={number} defaultValue=""/>
    );
    return (
        <div className='Result'>
            {ResultItems}
        </div>
    );
};
export default Result;

